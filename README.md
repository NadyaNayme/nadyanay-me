# Nadyanay.me

Personal website handwritten with public facing files being ran through Gulp. Gulp is set up to minify the CSS and HTML, sign the CSS for integrity checking, and minimize all of my images.

## Privacy

All images open in a new tab and all external links open in a new tab by default and are told not to send a referrer in the header. I do not use an anonymizing service so that you will know exactly where the link will take you to. I don't load any 3rd party Javascript and currently don't even run any Javascript at all. I will never add user tracking of any type because I simply do not care. I don't care how popular the site is or isn't - it exists for my personal satisfaction.

If any external links are missing the rel="external noreferrer" let me know and I'll update it ASAP.
